package me.shadow5353.pvpbattle;

import net.amigocraft.mglib.api.Minigame;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin{
	
	public Minigame mg;
	private static Plugin plugin;
	
	public void onEnable(){
		plugin = this;
		SettingsManager.getInstance().setup(this);
		getCommand("battle").setExecutor(new CommandManager());
		mg = Minigame.registerPlugin(this);
		if (!Bukkit.getPluginManager().isPluginEnabled("MGLib"));
	}
	
	public void onDisable(){
		plugin = null;
	}
	
	public static Plugin getPlugin() {
		return plugin;
	}

}

package me.shadow5353.pvpbattle.cmds;

import me.shadow5353.pvpbattle.MessageManager;
import me.shadow5353.pvpbattle.SubCommand;
import net.amigocraft.mglib.api.Minigame;

import org.bukkit.entity.Player;

public class Leave extends SubCommand{
	
	public Minigame mg;
	MessageManager msg = MessageManager.getInstance();
	
	public void onCommand(Player p, String[] args){
		if(!(p.hasPermission("pvpbattle.leave"))){
			msg.perm(p);
			return;
		}
		
		try {
			msg.good(p, "You have left " + mg.getMGPlayer(p.getName()).getArena());
			mg.getMGPlayer(p.getName()).removeFromRound();
		} catch (Exception ex){
			msg.error(p, "You are not in a arena!");
		}
		return;
	}
	
	public Leave(){
		super("Leave a arena", "", "");
	}

}

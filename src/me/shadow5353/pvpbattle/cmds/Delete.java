package me.shadow5353.pvpbattle.cmds;

import me.shadow5353.pvpbattle.MessageManager;
import me.shadow5353.pvpbattle.SubCommand;
import net.amigocraft.mglib.api.Minigame;
import net.amigocraft.mglib.exception.NoSuchArenaException;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Delete extends SubCommand{
	
	public Minigame mg;
	MessageManager msg = MessageManager.getInstance();
	
	public void onCommand(Player p, String[] args){
		if(!(p.hasPermission("pvpbattle.delete"))){
			msg.perm(p);
			return;
		}
		if(!(args.length == 1)){
			msg.error(p, "Not enough arguments: " + ChatColor.YELLOW + "/battle delete [Name]");
			return;
		}
		try{
			mg.deleteArena(args[1]);
			msg.good(p, "Successfully deleted arena " + args[1] + "!");
		} catch (NoSuchArenaException ex){
			msg.error(p, "An arena do not exist with that name!");
		}
		return;
	}
	
	public Delete(){
		super("Delete an arena", "[Name]", "");
	}

}

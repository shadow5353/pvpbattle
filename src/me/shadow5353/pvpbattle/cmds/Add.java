package me.shadow5353.pvpbattle.cmds;

import me.shadow5353.pvpbattle.MessageManager;
import me.shadow5353.pvpbattle.SubCommand;
import net.amigocraft.mglib.api.Minigame;
import net.amigocraft.mglib.exception.InvalidLocationException;
import net.amigocraft.mglib.exception.NoSuchArenaException;

import org.bukkit.entity.Player;

public class Add extends SubCommand{
	
	public Minigame mg;
	MessageManager msg = MessageManager.getInstance();
	
	public void onCommand(Player p, String[] args){
		if(!(p.hasPermission("pvpbattle.add"))){
			msg.perm(p);
			return;
		}
		if(args.length == 1){
			msg.info(p, "Here is a list of things to add to the arena!");
			return;
		}
		if(args[1].equalsIgnoreCase("spawn")){
			try {
				mg.getArenaFactory(args[2]).addSpawn(p.getLocation());
				msg.good(p, "Added spawn for " + args[2] + "!");
			}  
			catch (NoSuchArenaException ex) {
				msg.error(p, args[2] + " do not exist!");
			}
			catch (InvalidLocationException ex) {
				msg.error(p, "That location is not valid to add spawn on!");
			}
			return;
		}
	}
	
	public Add (){
		super("Add menu", "[Spawn]", "");
	}

}

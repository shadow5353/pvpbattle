package me.shadow5353.pvpbattle.cmds;

import me.shadow5353.pvpbattle.MessageManager;
import me.shadow5353.pvpbattle.SubCommand;
import net.amigocraft.mglib.api.Minigame;
import net.amigocraft.mglib.exception.ArenaExistsException;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Create extends SubCommand{
	
	public Minigame mg;
	MessageManager msg = MessageManager.getInstance();
	
	public void onCommand(Player p, String[] args){
		if(!(p.hasPermission("pvpbattle.create"))){
			msg.perm(p);
			return;
		}
		if(!(args.length == 1)){
			msg.error(p, "Not enough arguments: " + ChatColor.YELLOW + "/battle create [Name]");
			return;
		}
		try{
			mg.createArena(args[1], p.getLocation());
			msg.good(p, "Successfully created arena " + args[1] + "!");
		} catch (ArenaExistsException ex){
			msg.error(p, "An arena already exist with the same name!");
		}
		return;
	}
	
	public Create(){
		super("Create a arena", "[name]", "");
	}

}

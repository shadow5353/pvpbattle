package me.shadow5353.pvpbattle.cmds;

import me.shadow5353.pvpbattle.MessageManager;
import me.shadow5353.pvpbattle.SubCommand;
import net.amigocraft.mglib.api.Minigame;
import net.amigocraft.mglib.api.Round;
import net.amigocraft.mglib.exception.NoSuchArenaException;
import net.amigocraft.mglib.exception.PlayerOfflineException;
import net.amigocraft.mglib.exception.PlayerPresentException;
import net.amigocraft.mglib.exception.RoundFullException;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Join extends SubCommand{
	
	public Minigame mg;
	MessageManager msg = MessageManager.getInstance();
	
	public void onCommand(Player p, String[] args){
		if(!(p.hasPermission("pvpbattle.join"))){
			msg.perm(p);
			return;
		}
		if(!(args.length == 1)){
			msg.error(p, "Not enough arguments: " + ChatColor.YELLOW + "/battle join [Name]");
			return;
		}
		try {
			Round r = mg.getRound(args[1]);
			if(r == null)
				mg.createRound(args[1]);
			try {
				r.addPlayer(p.getName());
				msg.good(p, "Successfully joined arena " + args[1] + "!");
			} catch (PlayerPresentException ex){
				msg.error(p, "You are in " + mg.getMGPlayer(p.getName()).getArena() + "!");
			} catch (PlayerOfflineException e) {
				msg.error(p, "This player is offline!");
			} catch (RoundFullException e) {
				msg.error(p, "The arena is full in " + args[1] + "!");
			}
		} catch (NoSuchArenaException ex){
			msg.error(p, "That arena do not exist");
		}
	}
	
	public Join(){
		super("Join a arena", "[Name]", "");
	}

}

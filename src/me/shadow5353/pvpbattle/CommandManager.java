package me.shadow5353.pvpbattle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;

import me.shadow5353.pvpbattle.cmds.Create;
import me.shadow5353.pvpbattle.cmds.Delete;
import me.shadow5353.pvpbattle.cmds.Join;
import me.shadow5353.pvpbattle.cmds.Leave;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandManager implements CommandExecutor {

	private ArrayList<SubCommand> cmds = new ArrayList<SubCommand>();
	SettingsManager s = SettingsManager.getInstance();
	
	public CommandManager() {
		cmds.add(new Create());
		cmds.add(new Delete());
		cmds.add(new Join());
		cmds.add(new Leave());
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "Only players can use PvP Battle!");
			return true;
		}
		
		Player p = (Player) sender;
		if (cmd.getName().equalsIgnoreCase("battle")) {
			if (args.length == 0) {
				p.sendMessage(ChatColor.GOLD + "--------------------------------------------------");
				p.sendMessage(ChatColor.GOLD + "--------------------------------------------------");
				return true;
			}
			
			SubCommand c = getCommand(args[0]);
			
			if (c == null) {
				sender.sendMessage(ChatColor.RED + "That command doesn't exist!");
				return true;
			}
			
			Vector<String> a = new Vector<String>(Arrays.asList(args));
			a.remove(0);
			args = a.toArray(new String[a.size()]);
			
			c.onCommand(p, args);
			
			return true;
		}
		return true;
	}
	
	private SubCommand getCommand(String name) {
		for (SubCommand cmd : cmds) {
			if (cmd.getClass().getSimpleName().equalsIgnoreCase(name)) return cmd;
			for (String alias : cmd.getAliases()) if (name.equalsIgnoreCase(alias)) return cmd;
		}
		return null;
	}
}